﻿Imports System.Data
Imports System.IO
Imports ProductionControl.DataSetPDCS

Public Class PdcsAPI

    Public Function GetCurrentDate()

        Return String.Format("{0:yyyyMMdd}", DateTime.Now)

    End Function

    Public Function GetCurrentTime()

        Return String.Format("{0:HH:mm:ss}", DateTime.Now)

    End Function

    Public Sub ErrorLog(ByVal programCode As String, ByVal errorMsg As String)

        Dim dsPDCS As New DataSetPDCS
        Dim dtPdcsErrorMsg As New DataSetPDCS.PdcsErrorMsgDataTable
        Dim adPdcsErrorMsg As New DataSetPDCSTableAdapters.PdcsErrorMsgTableAdapter

        Dim strHostName As String
        strHostName = System.Net.Dns.GetHostName()

        adPdcsErrorMsg.InsertErrorMsg(strHostName, programCode, errorMsg, System.DateTime.Now, "")

    End Sub

    Public Function GetMaterialList(ByVal woNo As String)

        Dim dsPDCS As New DataSetPDCS
        Dim dtListMaterial As New DataSetPDCS.WoMatTrDataTable
        Dim adListMaterial As New DataSetPDCSTableAdapters.WoMatTrTableAdapter

        dtListMaterial = dsPDCS.WoMatTr
        adListMaterial.FillByWoNo(dtListMaterial, woNo)

        Return dtListMaterial
    End Function

    Public Function GetMaterialList(ByVal woNo As String, ByVal seqNo As String)

        Dim dsPDCS As New DataSetPDCS
        Dim dtListMaterial As New DataSetPDCS.WoMatTrDataTable
        Dim adListMaterial As New DataSetPDCSTableAdapters.WoMatTrTableAdapter

        dtListMaterial = dsPDCS.WoMatTr
        adListMaterial.FillByWoAndSeq(dtListMaterial, woNo, seqNo)

        Return dtListMaterial
    End Function

    Public Function GetMaterialListWoAndLine(ByVal woNo As String, ByVal seqNo As String, ByVal pdLine As String)

        Dim dsPDCS As New DataSetPDCS
        Dim dtListMaterial As New DataSetPDCS.WoMatTrDataTable
        Dim adListMaterial As New DataSetPDCSTableAdapters.WoMatTrTableAdapter

        dtListMaterial = dsPDCS.WoMatTr
        adListMaterial.FillByWoAndLine(dtListMaterial, pdLine, woNo, seqNo)

        Return dtListMaterial
    End Function

    Public Function GetMcLocation(ByVal pdLine As String)

        Dim dsPC As New DataSetPC
        Dim dtListLocCb As New DataSetPC.ListMcDataTable
        Dim adListLocCb As New DataSetPCTableAdapters.ListMcTableAdapter

        dtListLocCb = dsPC.ListMc
        adListLocCb.FillByLine(dtListLocCb, pdLine)

        Return dtListLocCb
    End Function

    Public Function GetIssueMsg()

        Dim dsPDCS As New DataSetPDCS
        Dim dtListMsgCb As New DataSetPDCS.PdcsIssueMsgDataTable
        Dim adListMsgCb As New DataSetPDCSTableAdapters.PdcsIssueMsgTableAdapter

        dtListMsgCb = dsPDCS.PdcsIssueMsg
        adListMsgCb.Fill(dtListMsgCb)

        Return dtListMsgCb
    End Function

    Public Function GetLine()

        Dim dsPDCS As New DataSetPDCS
        Dim dtListLine As New DataSetPDCS.PdcsLineDataTable
        Dim adListLine As New DataSetPDCSTableAdapters.PdcsLineTableAdapter

        dtListLine = dsPDCS.PdcsLine
        adListLine.FillAll(dtListLine)

        Return dtListLine
    End Function

    Public Function GetNewSeqMaterial()

        Dim dsPDCS As New DataSetPDCS
        Dim dtPdcsWoMatTr As New DataSetPDCS.PdcsWoMatTrDataTable
        Dim adPdcsWoMatTr As New DataSetPDCSTableAdapters.PdcsWoMatTrTableAdapter

        Dim iSeqNo As Integer

        iSeqNo = adPdcsWoMatTr.GetSeqNo()
        If iSeqNo = 0 Then
            iSeqNo = 1
        End If

        Return iSeqNo
    End Function

    Public Function ChkPrintNo(ByVal woNo As String)

        Dim dsPDCS As New DataSetPDCS
        Dim dtPdcsWoMatTr As New DataSetPDCS.PdcsWoMatTrDataTable
        Dim adPdcsWoMatTr As New DataSetPDCSTableAdapters.PdcsWoMatTrTableAdapter

        Dim iPrintNo As Integer

        iPrintNo = adPdcsWoMatTr.ChkPrintNo(woNo)

        Return iPrintNo

    End Function

    Public Sub AddMaterialPrintLog(ByVal seqNo As Integer, ByVal termNo As String, ByVal companyCode As String, ByVal factoryCode As String, ByVal woNo As String, ByVal itemCd As String, ByVal bomQty As Integer, ByVal actQty As Integer, ByVal issueMsg As String, ByVal location As String)

        Dim dsPDCS As New DataSetPDCS
        Dim dtPdcsWoMatTr As New DataSetPDCS.PdcsWoMatTrDataTable
        Dim adPdcsWoMatTr As New DataSetPDCSTableAdapters.PdcsWoMatTrTableAdapter

        adPdcsWoMatTr.InsertWoTr(seqNo, termNo, companyCode, factoryCode, woNo, itemCd, bomQty, actQty, GetCurrentDate(), GetCurrentTime(), issueMsg, location)

    End Sub


End Class
