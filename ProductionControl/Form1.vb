﻿Imports System.Data
Imports System.IO
Imports ProductionControl.DataSetPC
Imports ProductionControl.DataSetPDCS
Imports ProductionControl.PdcsAPI

Public Class Form1

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim dsPC As New DataSetPC
        Dim dtListPack As New DataSetPC.ListPackDataTable
        Dim adListPack As New DataSetPCTableAdapters.ListPackTableAdapter

        Dim apiPdcs As New PdcsAPI
        Dim dtListLine As DataTable

        dgMaterial.AutoGenerateColumns = False

        ' Pack Type
        Try

            dtListPack = dsPC.ListPack

            adListPack.FillByAll(dtListPack)
            If dtListPack.Rows.Count > 0 Then
                'cbPackType
                With cbPackType

                    .DataSource = dtListPack
                    .DisplayMember = "pack_desc"
                    .ValueMember = "i_pack_cd"
                    .Text = "no specify"

                End With
            End If

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try

        Try

            dtListLine = apiPdcs.GetLine()
            With cbLineInMat

                .DataSource = dtListLine
                .DisplayMember = "pdline"
                .ValueMember = "pdline"
                '.Text = "--Select--"
            End With

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try

    End Sub
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim dsPC As New DataSetPC

        Dim dtItemInfo As New DataSetPC.ItemInfoDataTable
        Dim adItemInfo As New DataSetPCTableAdapters.ItemInfoTableAdapter

        Dim dtListItem As New DataSetPC.ListItemDataTable
        Dim adListItem As New DataSetPCTableAdapters.ListItemTableAdapter

        Dim dtListPack As New DataSetPC.ListPackDataTable
        Dim adListPack As New DataSetPCTableAdapters.ListPackTableAdapter

        Dim stItemCd As String
        Dim stWiNo As String
        Dim stIndContent As String
        Dim stMcInfo As String
        Dim dgvc As New DataGridViewComboBoxCell

        Dim dsPDCS As New DataSetPDCS

        Dim dtPdcsBomList As New DataSetPDCS.PdcsBomListDataTable
        Dim adPdcsBomList As New DataSetPDCSTableAdapters.PdcsBomListTableAdapter

        ' Item Info
        Try

            dtItemInfo = dsPC.ItemInfo

            adItemInfo.FillByWoNo(dtItemInfo, tbWoWoNo.Text + "%", tbWoCustCd.Text + "%")
            If dtItemInfo.Rows.Count > 0 Then



                tbPartNo.Text = dtItemInfo.DataSet.Tables("ItemInfo").Rows(0).Item("i_trade_item_cd")
                tbPartDesc.Text = dtItemInfo.DataSet.Tables("ItemInfo").Rows(0).Item("i_trade_item_desc")
                tbItemNo.Text = dtItemInfo.DataSet.Tables("ItemInfo").Rows(0).Item("i_item_cd")

            End If

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try

        ' Pack Type
        Try

            dtListPack = dsPC.ListPack

            adListPack.FillByAll(dtListPack)
            If dtListPack.Rows.Count > 0 Then
                'cbPackType
                With cbPackType

                    .DataSource = dtListPack
                    .DisplayMember = "pack_desc"
                    .ValueMember = "i_pack_cd"
                    .Text = "no specify"

                End With
            End If

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try

        ' Data Grid
        DataGridView1.DataSource = Nothing
        DataGridView1.AutoGenerateColumns = False

        Try

            dtPdcsBomList = dsPDCS.PdcsBomList

            adPdcsBomList.Fill(dtPdcsBomList, tbItemNo.Text.Trim())
            If dtPdcsBomList.Rows.Count > 0 Then
                DataGridView1.DataSource = dtPdcsBomList.DataSet.Tables("PdcsBomList")
            End If

            For row_index = 0 To DataGridView1.Rows.Count - 2

                Dim dtListWiCb As New DataSetPC.ListWiDataTable
                Dim adListWiCb As New DataSetPCTableAdapters.ListWiTableAdapter

                dtListWiCb = dsPC.ListWi
                stItemCd = DataGridView1.Rows(row_index).Cells(1).Value().ToString().Trim()
                adListWiCb.FillByItemCd(dtListWiCb, stItemCd)

                If dtListWiCb.Rows.Count > 0 Then
                    dgvc = DataGridView1.Rows(row_index).Cells(4)

                    For row_cb = 0 To dtListWiCb.Rows.Count - 1

                        dgvc.Items.Add(dtListWiCb.DataSet.Tables("ListWi").Rows(row_cb).Item("wi_no"))
                        stWiNo = dtListWiCb.DataSet.Tables("ListWi").Rows(row_cb).Item("wi_no")

                        DataGridView1.Rows(row_index).Cells(4).Value = stWiNo
                    Next

                End If

                Dim dtListMcCb As New DataSetPC.ListMcDataTable
                Dim adListMcCb As New DataSetPCTableAdapters.ListMcTableAdapter

                dtListMcCb = dsPC.ListMc
                'adListMcCb.FillAll(dtListMcCb)
                stIndContent = DataGridView1.Rows(row_index).Cells(10).Value().ToString().Trim()
                adListMcCb.FillByIndContent(dtListMcCb, stIndContent.Trim())

                If dtListMcCb.Rows.Count > 0 Then
                    dgvc = DataGridView1.Rows(row_index).Cells(5)

                    For row_cb = 0 To dtListMcCb.Rows.Count - 1

                        dgvc.Items.Add(dtListMcCb.DataSet.Tables("ListMc").Rows(row_cb).Item("mc_cd") + ":" + dtListMcCb.DataSet.Tables("ListMc").Rows(row_cb).Item("mc_desc"))
                        stMcInfo = dtListMcCb.DataSet.Tables("ListMc").Rows(row_cb).Item("mc_cd") + ":" + dtListMcCb.DataSet.Tables("ListMc").Rows(row_cb).Item("mc_desc")
                        DataGridView1.Rows(row_index).Cells(5).Value = stMcInfo
                    Next

                End If

            Next

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try
    End Sub
    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbWoCustCd.KeyPress
        Dim dsPC As New DataSetPC

        Dim dtItemInfo As New DataSetPC.ItemInfoDataTable
        Dim adItemInfo As New DataSetPCTableAdapters.ItemInfoTableAdapter

        Dim dtListItem As New DataSetPC.ListItemDataTable
        Dim adListItem As New DataSetPCTableAdapters.ListItemTableAdapter

        Dim dtListPack As New DataSetPC.ListPackDataTable
        Dim adListPack As New DataSetPCTableAdapters.ListPackTableAdapter

        Dim stItemCd As String
        Dim stWiNo As String
        Dim stIndContent As String
        Dim stMcInfo As String
        Dim dgvc As New DataGridViewComboBoxCell

        Dim dsPDCS As New DataSetPDCS

        Dim dtListItemPDCS As New DataSetPDCS.PdcsBomListDataTable
        Dim adListItemPDCS As New DataSetPDCSTableAdapters.PdcsBomListTableAdapter

        ' Item Info
        Try

            dtItemInfo = dsPC.ItemInfo

            adItemInfo.FillByWoNo(dtItemInfo, tbWoWoNo.Text + "%", tbWoCustCd.Text + "%")
            If dtItemInfo.Rows.Count > 0 Then



                tbPartNo.Text = dtItemInfo.DataSet.Tables("ItemInfo").Rows(0).Item("i_trade_item_cd")
                tbPartDesc.Text = dtItemInfo.DataSet.Tables("ItemInfo").Rows(0).Item("i_trade_item_desc")
                tbItemNo.Text = dtItemInfo.DataSet.Tables("ItemInfo").Rows(0).Item("i_item_cd")

            End If

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try

        ' Pack Type
        Try

            dtListPack = dsPC.ListPack

            adListPack.FillByAll(dtListPack)
            If dtListPack.Rows.Count > 0 Then
                'cbPackType
                With cbPackType

                    .DataSource = dtListPack
                    .DisplayMember = "pack_desc"
                    .ValueMember = "i_pack_cd"
                    .Text = "no specify"

                End With
            End If

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try

        ' Data Grid
        DataGridView1.DataSource = Nothing
        DataGridView1.AutoGenerateColumns = False

        Try

            dtListItemPDCS = dsPDCS.PdcsBomList

            adListItemPDCS.Fill(dtListItemPDCS, tbItemNo.Text.Trim())
            If dtListItemPDCS.Rows.Count > 0 Then
                DataGridView1.DataSource = dtListItemPDCS.DataSet.Tables("PdcsBomList")
            End If

            For row_index = 0 To DataGridView1.Rows.Count - 2

                Dim dtListWiCb As New DataSetPC.ListWiDataTable
                Dim adListWiCb As New DataSetPCTableAdapters.ListWiTableAdapter

                dtListWiCb = dsPC.ListWi
                stItemCd = DataGridView1.Rows(row_index).Cells(1).Value().ToString().Trim()
                adListWiCb.FillByItemCd(dtListWiCb, stItemCd)

                If dtListWiCb.Rows.Count > 0 Then
                    dgvc = DataGridView1.Rows(row_index).Cells(4)

                    For row_cb = 0 To dtListWiCb.Rows.Count - 1

                        dgvc.Items.Add(dtListWiCb.DataSet.Tables("ListWi").Rows(row_cb).Item("wi_no"))
                        stWiNo = dtListWiCb.DataSet.Tables("ListWi").Rows(row_cb).Item("wi_no")

                        DataGridView1.Rows(row_index).Cells(4).Value = stWiNo
                    Next

                End If

                Dim dtListMcCb As New DataSetPC.ListMcDataTable
                Dim adListMcCb As New DataSetPCTableAdapters.ListMcTableAdapter

                dtListMcCb = dsPC.ListMc
                'adListMcCb.FillAll(dtListMcCb)
                stIndContent = DataGridView1.Rows(row_index).Cells(10).Value().ToString().Trim()
                adListMcCb.FillByIndContent(dtListMcCb, stIndContent.Trim())

                If dtListMcCb.Rows.Count > 0 Then
                    dgvc = DataGridView1.Rows(row_index).Cells(5)

                    For row_cb = 0 To dtListMcCb.Rows.Count - 1

                        dgvc.Items.Add(dtListMcCb.DataSet.Tables("ListMc").Rows(row_cb).Item("mc_cd") + ":" + dtListMcCb.DataSet.Tables("ListMc").Rows(row_cb).Item("mc_desc"))
                        stMcInfo = dtListMcCb.DataSet.Tables("ListMc").Rows(row_cb).Item("mc_cd") + ":" + dtListMcCb.DataSet.Tables("ListMc").Rows(row_cb).Item("mc_desc")
                        DataGridView1.Rows(row_index).Cells(5).Value = stMcInfo
                    Next

                End If

            Next

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try
    End Sub

    Private Sub btSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSelect.Click
        Dim dsPC As New DataSetPC

        Dim dtListWi As New DataSetPC.ListWiDataTable
        Dim adListWi As New DataSetPCTableAdapters.ListWiTableAdapter

        ' Data Grid
        dgWi.DataSource = Nothing
        dgWi.AutoGenerateColumns = False

        Try

            dtListWi = dsPC.ListWi

            If tbTab2WiNo.Text = "" Then
                tbTab2WiNo.Text = "%"
            End If

            adListWi.FillByWiNo(dtListWi, tbTab2WiNo.Text)
            If dtListWi.Rows.Count > 0 Then

                dgWi.DataSource = dtListWi.DataSet.Tables("ListWi")
            End If

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try
    End Sub

    Private Sub dgWi_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgWi.CellContentClick
        Dim dsPC As New DataSetPC

        Dim dtListWi As New DataSetPC.ListWiDataTable
        Dim adListWi As New DataSetPCTableAdapters.ListWiTableAdapter

        Dim senderGrid = DirectCast(sender, DataGridView)

        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn And e.RowIndex >= 0 Then
            'TODO - Button Clicked - Execute Code Here

            If senderGrid.Columns(e.ColumnIndex).Name = "browse_file_row" Then
                Try

                    ' \\10.152.1.93\ppap-document\C 001 - ISUZU MORTORS (THAILAND) CO.,LTD\C001-003 - 897252-0831

                    Dim OpenFileDialogWi As New OpenFileDialog()
                    'OpenFileDialogWi.Filter = "Cursor Files|*xls"
                    OpenFileDialogWi.Filter = "Cursor Files|*JPG"
                    OpenFileDialogWi.Title = "Select a Cursor File"

                    If OpenFileDialogWi.ShowDialog() = DialogResult.OK Then

                        Dim sFilename = OpenFileDialogWi.FileName()
                        dgWi.Rows(e.RowIndex).Cells(3).Value = sFilename

                    End If

                Catch ex As Exception

                    MsgBox(ex.ToString)

                End Try
            End If

            If senderGrid.Columns(e.ColumnIndex).Name = "save_row" Then

                Try
                    adListWi.DeleteWi(dgWi.Rows(e.RowIndex).Cells(0).Value)
                Catch ex As Exception
                    'MsgBox(ex.ToString)
                End Try
                Try
                    adListWi.InsertWi(dgWi.Rows(e.RowIndex).Cells(0).Value, dgWi.Rows(e.RowIndex).Cells(1).Value, dgWi.Rows(e.RowIndex).Cells(3).Value, dgWi.Rows(e.RowIndex).Cells(2).Value)
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try

                MsgBox("Save was successful.")

            End If

            If senderGrid.Columns(e.ColumnIndex).Name = "wi_delete_row" Then

                If MsgBox("Do you want to delete?", MsgBoxStyle.OkCancel, "") = MsgBoxResult.Ok Then
                    Try
                        adListWi.DeleteWi(dgWi.Rows(e.RowIndex).Cells(0).Value)
                    Catch ex As Exception
                        'MsgBox(ex.ToString)
                    End Try

                    MsgBox("Delete was successful.")
                End If

            End If

            ' Data Grid
            dgWi.DataSource = Nothing
            dgWi.AutoGenerateColumns = False

            Try

                dtListWi = dsPC.ListWi

                If tbTab2WiNo.Text = "" Then
                    tbTab2WiNo.Text = "%"
                End If

                adListWi.FillByWiNo(dtListWi, tbTab2WiNo.Text)
                If dtListWi.Rows.Count > 0 Then

                    dgWi.DataSource = dtListWi.DataSet.Tables("ListWi")
                End If

            Catch ex As Exception

                MsgBox(ex.ToString)

            End Try

        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'dgWi.Rows.Add("1", "2", "3", "4")
        
        Dim dsPC As New DataSetPC

        Dim dtListWi As New DataSetPC.ListWiDataTable
        Dim adListWi As New DataSetPCTableAdapters.ListWiTableAdapter

        ' Data Grid
        dgWi.DataSource = Nothing
        dgWi.AutoGenerateColumns = False

        Try
            
            dtListWi = dsPC.ListWi

            adListWi.FillByWiNo(dtListWi, tbTab2WiNo.Text)

            Dim drListWi As DataRow
            drListWi = dtListWi.NewRow
            drListWi(0) = ""
            drListWi(1) = ""
            drListWi(2) = ""
            dtListWi.Rows.InsertAt(drListWi, dtListWi.Rows.Count)

            If dtListWi.Rows.Count > 0 Then

                dgWi.DataSource = dtListWi.DataSet.Tables("ListWi")

            End If

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbPackType.SelectedIndexChanged

    End Sub

    Private Sub btMcSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btMcSelect.Click
        Dim dsPC As New DataSetPC

        Dim dtListMc As New DataSetPC.ListMcDataTable
        Dim adListMc As New DataSetPCTableAdapters.ListMcTableAdapter

        ' Data Grid
        dgMc.DataSource = Nothing
        dgMc.AutoGenerateColumns = False

        Try

            dtListMc = dsPC.ListMc

            If tbTab3McNo.Text = "" Then
                tbTab3McNo.Text = "%"
            End If

            adListMc.FillByMcCd(dtListMc, tbTab3McNo.Text)

            If dtListMc.Rows.Count > 0 Then

                dgMc.DataSource = dtListMc.DataSet.Tables("ListMc")

            End If

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try
    End Sub

    Private Sub btMcAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btMcAdd.Click
        Dim dsPC As New DataSetPC

        Dim dtListMc As New DataSetPC.ListMcDataTable
        Dim adListMc As New DataSetPCTableAdapters.ListMcTableAdapter

        ' Data Grid
        dgMc.DataSource = Nothing
        dgMc.AutoGenerateColumns = False

        Try

            dtListMc = dsPC.ListMc

            adListMc.FillByMcCd(dtListMc, tbTab3McNo.Text)

            Dim drListMc As DataRow
            drListMc = dtListMc.NewRow
            drListMc(0) = ""
            drListMc(1) = ""
            drListMc(2) = ""
            drListMc(3) = ""
            drListMc(4) = ""
            drListMc(5) = ""
            dtListMc.Rows.InsertAt(drListMc, dtListMc.Rows.Count)

            If dtListMc.Rows.Count > 0 Then

                dgMc.DataSource = dtListMc.DataSet.Tables("ListMc")

            End If

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim dsPC As New DataSetPC

        Dim dtListWi As New DataSetPC.ListWiDataTable
        Dim adListWi As New DataSetPCTableAdapters.ListWiTableAdapter

        'Form2.tbMatWoNo.Text = tbWoWoNo.Text

        For row_mc = 0 To DataGridView1.Rows.Count - 2
            Dim stMcCode = Split(DataGridView1.Rows(row_mc).Cells(5).Value, ":").GetValue(0)

            If stMcCode = "CT-045" Then
                dtListWi = dsPC.ListWi
                adListWi.FillByWiNo(dtListWi, DataGridView1.Rows(row_mc).Cells(4).Value)
                If dtListWi.Rows.Count > 0 Then
                    For row_wi = 0 To dtListWi.Rows.Count - 1
                        Form2.PictureBox1.ImageLocation() = dtListWi.DataSet.Tables("ListWi").Rows(row_wi).Item("wi_filename")
                    Next
                End If

            End If
        Next

        Form2.Show()

    End Sub

    Private Sub dgMc_CellContentClick1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgMc.CellContentClick
        Dim dsPC As New DataSetPC

        Dim dtListMc As New DataSetPC.ListMcDataTable
        Dim adListMc As New DataSetPCTableAdapters.ListMcTableAdapter

        Dim senderGrid = DirectCast(sender, DataGridView)

        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn And e.RowIndex >= 0 Then
            If senderGrid.Columns(e.ColumnIndex).Name = "mc_save_row" Then

                Try
                    adListMc.DeleteByMcCd(dgMc.Rows(e.RowIndex).Cells(0).Value)
                Catch ex As Exception
                    'MsgBox(ex.ToString)
                End Try
                Try
                    ' mc_cd,mc_desc,mc_status,ind_contents,ind_cont_desc,begin_lot
                    adListMc.InsertMc(dgMc.Rows(e.RowIndex).Cells(0).Value, dgMc.Rows(e.RowIndex).Cells(1).Value, dgMc.Rows(e.RowIndex).Cells(4).Value, dgMc.Rows(e.RowIndex).Cells(2).Value, dgMc.Rows(e.RowIndex).Cells(3).Value, dgMc.Rows(e.RowIndex).Cells(5).Value)
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try

                MsgBox("Save was successful.")

            End If
            If senderGrid.Columns(e.ColumnIndex).Name = "mc_delete_row" Then
                If MsgBox("Do you want to delete?", MsgBoxStyle.OkCancel, "") = MsgBoxResult.Ok Then

                    Try
                        adListMc.DeleteByMcCd(dgMc.Rows(e.RowIndex).Cells(0).Value)
                    Catch ex As Exception
                        'MsgBox(ex.ToString)
                    End Try

                    MsgBox("Delete was successful.")
                End If

            End If

            ' Data Grid
            dgMc.DataSource = Nothing
            dgMc.AutoGenerateColumns = False

            Try

                dtListMc = dsPC.ListMc

                If tbTab3McNo.Text = "" Then
                    tbTab3McNo.Text = "%"
                End If

                adListMc.FillByMcCd(dtListMc, tbTab3McNo.Text)

                If dtListMc.Rows.Count > 0 Then

                    dgMc.DataSource = dtListMc.DataSet.Tables("ListMc")

                End If

            Catch ex As Exception

                MsgBox(ex.ToString)

            End Try

        End If
    End Sub

    Private Sub tbWoQty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbWoQty.TextChanged

        Dim dsPDCS As New DataSetPDCS
        Dim dtListItemPDCS As New DataSetPDCS.PdcsBomListDataTable
        Dim adListItemPDCS As New DataSetPDCSTableAdapters.PdcsBomListTableAdapter

        dtListItemPDCS = dsPDCS.PdcsBomList
        adListItemPDCS.Fill(dtListItemPDCS, tbItemNo.Text.Trim())

        If tbWoQty.Text > 0 Then
            Try
                
                For row_index = 0 To DataGridView1.Rows.Count - 1

                    DataGridView1.Rows(row_index).Cells(2).Value = Format((DataGridView1.Rows(row_index).Cells(9).Value * tbWoQty.Text), "0.00")

                Next

            Catch ex As Exception

                MsgBox(ex.ToString)

            End Try
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        Dim dsPDCS As New DataSetPDCS
        Dim dtListMaterial As New DataSetPDCS.WoMatTrDataTable
        Dim adListMaterial As New DataSetPDCSTableAdapters.WoMatTrTableAdapter

        tbMatWoNo.Text = tbWoWoNo.Text

        ' Data Grid
        dgMaterial.DataSource = Nothing
        dgMaterial.AutoGenerateColumns = False

        Try

            dtListMaterial = dsPDCS.WoMatTr
            adListMaterial.FillByWoNo(dtListMaterial, tbMatWoNo.Text.Trim())

            If dtListMaterial.Rows.Count > 0 Then

                dgMaterial.DataSource = dtListMaterial.DataSet.Tables("WoMatTr")

            End If

        Catch ex As Exception

            MsgBox(ex.ToString)

        End Try

        TabControl1.SelectTab("TabPage4")
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        Dim dtListMaterial As DataTable
        Dim dtListMsgCb As DataTable

        Dim apiPdcs As New PdcsAPI

        Dim dgvc As New DataGridViewComboBoxCell

        ' Data Grid
        dgMaterial.DataSource = Nothing
        dgMaterial.AutoGenerateColumns = False

        If tbMatWoNo.Text.Trim() = "" Then
            MsgBox("Please input WO No.")
            apiPdcs.ErrorLog("MAT_SELECT", "Please input WO No.")
        Else
            Try

                dtListMaterial = apiPdcs.GetMaterialList(tbMatWoNo.Text.Trim())
                
                If dtListMaterial.Rows.Count > 0 Then

                    dgMaterial.DataSource = dtListMaterial.DataSet.Tables("WoMatTr")

                    For row_index = 0 To dgMaterial.Rows.Count - 1
                        
                        dtListMsgCb = apiPdcs.GetIssueMsg

                        If dtListMsgCb.Rows.Count > 0 Then
                            dgvc = dgMaterial.Rows(row_index).Cells(7)

                            For row_cb = 0 To dtListMsgCb.Rows.Count - 1

                                dgvc.Items.Add(dtListMsgCb.DataSet.Tables("PdcsIssueMsg").Rows(row_cb).Item("issue_desc"))
                            Next

                        End If
                    Next
                End If

            Catch ex As Exception

                MsgBox(ex.ToString)

            End Try
        End If
    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click

        Dim dtListMaterial As DataTable
        
        Dim iSeqNo As Integer
        Dim iPrintNo As Integer
        Dim sIssueMsg As String
        Dim sLocation As String
        Dim dgvcLoc As New DataGridViewComboBoxCell
        
        Dim apiPdcs As New PdcsAPI

        If tbMatWoNo.Text.Trim() = "" Then

            MsgBox("Please input WO No.")
            apiPdcs.ErrorLog("MAT_ISSUE", "Please input WO No.")

        Else

            Try

                dtListMaterial = apiPdcs.GetMaterialList(tbMatWoNo.Text.Trim())

                If dtListMaterial.Rows.Count > 0 Then

                    iSeqNo = apiPdcs.GetNewSeqMaterial()

                    iPrintNo = apiPdcs.ChkPrintNo(tbMatWoNo.Text.Trim())

                    sIssueMsg = dgMaterial.Rows(0).Cells(8).Value()
                    If sIssueMsg = "" And iPrintNo > 0 Then
                        MsgBox("เคยพิมพ์แล้ว กรุณาใส่ Remark")
                        apiPdcs.ErrorLog("MAT_ISSUE", "เคยพิมพ์แล้ว กรุณาใส่ Remark")
                    Else
                        If sIssueMsg = "" Then
                            sIssueMsg = ""
                        End If

                        'For row_index = 0 To dtListMaterial.Rows.Count - 1
                        For row_index = 0 To dgMaterial.RowCount - 2
                            dgvcLoc = dgMaterial.Rows(row_index).Cells(7)
                            sLocation = dgvcLoc.Value
                            apiPdcs.AddMaterialPrintLog(iSeqNo, dtListMaterial.DataSet.Tables("WoMatTr").Rows(row_index).Item("i_term_no"), dtListMaterial.DataSet.Tables("WoMatTr").Rows(row_index).Item("i_com_code"), dtListMaterial.DataSet.Tables("WoMatTr").Rows(row_index).Item("i_fac_cd"), dtListMaterial.DataSet.Tables("WoMatTr").Rows(row_index).Item("i_po_detail_no"), dtListMaterial.DataSet.Tables("WoMatTr").Rows(row_index).Item("I_SIM_ITEM_CD"), dtListMaterial.DataSet.Tables("WoMatTr").Rows(row_index).Item("bom_qty"), dtListMaterial.DataSet.Tables("WoMatTr").Rows(row_index).Item("act_qty"), sIssueMsg, sLocation)
                        Next

                        'dtListMaterial = apiPdcs.GetMaterialList(tbMatWoNo.Text.Trim(), iSeqNo.ToString())
                        dtListMaterial = apiPdcs.GetMaterialListWoAndLine(tbMatWoNo.Text.Trim(), iSeqNo.ToString(), "A")

                        Form3.CrystalReport31.SetDataSource(dtListMaterial)
                        Form3.CrystalReport31.Refresh()
                        Form3.CrystalReportViewer1.DisplayGroupTree = False
                        Form3.CrystalReportViewer1.ReportSource = Form3.CrystalReport31
                        Form3.CrystalReportViewer1.RefreshReport()
                        Form3.CrystalReportViewer1.Refresh()

                        Form3.Show()

                        tbMatWoNo.Text = ""
                        'dgMaterial.Rows.Clear()
                        dgMaterial.DataSource = Nothing

                    End If

                End If

                

            Catch ex As Exception

                MsgBox(ex.ToString)
                apiPdcs.ErrorLog("MAT_ISSUE", ex.ToString)

            End Try

            

        End If
        
    End Sub

    Private Sub dgMaterial_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgMaterial.CellContentClick
        
        If sender.Columns(e.ColumnIndex).Name = "mat_delete_row" Then
            If MsgBox("Do you want to delete?", MsgBoxStyle.OkCancel, "") = MsgBoxResult.Ok Then

                Try
                    dgMaterial.Rows.RemoveAt(e.RowIndex)
                Catch ex As Exception
                    'MsgBox(ex.ToString)
                End Try

                'MsgBox("Delete was successful.")
            End If

        End If

    End Sub

    Private Sub tbMatWoNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbMatWoNo.KeyPress
        If e.KeyChar = Chr(13) Then

            Dim dtListMaterial As DataTable
            Dim dtListMsgCb As DataTable
            Dim dtListLocCb As DataTable

            Dim apiPdcs As New PdcsAPI

            Dim dgvc As New DataGridViewComboBoxCell
            Dim dgvcLoc As New DataGridViewComboBoxCell
            Dim woNo As String

            ' Data Grid
            dgMaterial.DataSource = Nothing
            dgMaterial.AutoGenerateColumns = False

            If tbMatWoNo.Text.Trim() = "" Then
                tbMatWoNo.Focus()
            Else
                woNo = tbMatWoNo.Text.Trim()
                If woNo.StartsWith("WO") Then
                    tbMatWoNo.Text = woNo.Replace("WO", "")
                End If

                Try

                    dtListMaterial = apiPdcs.GetMaterialList(tbMatWoNo.Text.Trim())

                    If dtListMaterial.Rows.Count > 0 Then

                        dgMaterial.DataSource = dtListMaterial.DataSet.Tables("WoMatTr")

                        For row_index = 0 To dgMaterial.Rows.Count - 2

                            dtListLocCb = apiPdcs.GetMcLocation(cbLineInMat.SelectedValue())
                            If dtListLocCb.Rows.Count > 0 Then
                                dgvcLoc = dgMaterial.Rows(row_index).Cells(7)
                                For row_cb = 0 To dtListLocCb.Rows.Count - 1
                                    dgvcLoc.Items.Add(dtListLocCb.DataSet.Tables("ListMc").Rows(row_cb).Item("ind_contents"))
                                    If dtListMaterial.DataSet.Tables("WoMatTr").Rows(row_index).Item("i_ind_content").ToString.Trim() = dtListLocCb.DataSet.Tables("ListMc").Rows(row_cb).Item("ind_contents").ToString.Trim() Then
                                        dgMaterial.Rows(row_index).Cells(7).Value = dtListLocCb.DataSet.Tables("ListMc").Rows(row_cb).Item("ind_contents")
                                    End If
                                Next
                                'MsgBox(dtListMaterial.DataSet.Tables("WoMatTr").Rows(row_index).Item("i_ind_content"))
                            End If

                            dtListMsgCb = apiPdcs.GetIssueMsg
                            If dtListMsgCb.Rows.Count > 0 Then
                                dgvc = dgMaterial.Rows(row_index).Cells(8)
                                For row_cb = 0 To dtListMsgCb.Rows.Count - 1
                                    dgvc.Items.Add(dtListMsgCb.DataSet.Tables("PdcsIssueMsg").Rows(row_cb).Item("issue_desc"))
                                Next
                            End If
                        Next
                    End If

                Catch ex As Exception

                    MsgBox(ex.ToString)

                End Try
                
            End If
        End If
    End Sub

End Class
