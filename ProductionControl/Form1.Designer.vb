﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Nitto_Logo = New System.Windows.Forms.PictureBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.tbWoQty = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.cbPackType = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.i_level = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.i_upper_item_cd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.qty = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.unit = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.wi = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.machine = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.I_BATCH_SIZE_BUNSI = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.I_BATCH_SIZE_BUNBO = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.i_lower_item_cd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.I_SIZE_PER_ROOT = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ind_content = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label6 = New System.Windows.Forms.Label
        Me.tbItemNo = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.tbPartDesc = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.tbPartNo = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.tbWoCustCd = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.tbWoWoNo = New System.Windows.Forms.TextBox
        Me.TabPage5 = New System.Windows.Forms.TabPage
        Me.Button2 = New System.Windows.Forms.Button
        Me.btSelect = New System.Windows.Forms.Button
        Me.dgWi = New System.Windows.Forms.DataGridView
        Me.DataGridViewComboBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.filename = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.browse_file_row = New System.Windows.Forms.DataGridViewButtonColumn
        Me.save_row = New System.Windows.Forms.DataGridViewButtonColumn
        Me.wi_delete_row = New System.Windows.Forms.DataGridViewButtonColumn
        Me.Label8 = New System.Windows.Forms.Label
        Me.tbTab2WiNo = New System.Windows.Forms.TextBox
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.btMcAdd = New System.Windows.Forms.Button
        Me.btMcSelect = New System.Windows.Forms.Button
        Me.dgMc = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ind_contents = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ind_cont_desc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.begin_lot = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.mc_save_row = New System.Windows.Forms.DataGridViewButtonColumn
        Me.mc_delete_row = New System.Windows.Forms.DataGridViewButtonColumn
        Me.Label7 = New System.Windows.Forms.Label
        Me.tbTab3McNo = New System.Windows.Forms.TextBox
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.cbLineInMat = New System.Windows.Forms.ComboBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.tbMatWoNo = New System.Windows.Forms.TextBox
        Me.Button15 = New System.Windows.Forms.Button
        Me.Button12 = New System.Windows.Forms.Button
        Me.dgMaterial = New System.Windows.Forms.DataGridView
        Me.OpenFileDialogWi = New System.Windows.Forms.OpenFileDialog
        Me.wo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.I_SIM_ITEM_DESC = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lot_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.act_qty = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Location = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.remarks = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.mat_delete_row = New System.Windows.Forms.DataGridViewButtonColumn
        CType(Me.Nitto_Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        CType(Me.dgWi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgMc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        CType(Me.dgMaterial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Nitto_Logo
        '
        Me.Nitto_Logo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Nitto_Logo.ErrorImage = CType(resources.GetObject("Nitto_Logo.ErrorImage"), System.Drawing.Image)
        Me.Nitto_Logo.Image = CType(resources.GetObject("Nitto_Logo.Image"), System.Drawing.Image)
        Me.Nitto_Logo.Location = New System.Drawing.Point(12, 12)
        Me.Nitto_Logo.Name = "Nitto_Logo"
        Me.Nitto_Logo.Size = New System.Drawing.Size(140, 42)
        Me.Nitto_Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Nitto_Logo.TabIndex = 52
        Me.Nitto_Logo.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Navy
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label2.Font = New System.Drawing.Font("Courier New", 26.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LemonChiffon
        Me.Label2.Location = New System.Drawing.Point(162, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(397, 42)
        Me.Label2.TabIndex = 53
        Me.Label2.Text = "Production Control"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(11, 65)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(929, 436)
        Me.TabControl1.TabIndex = 54
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Button5)
        Me.TabPage1.Controls.Add(Me.Button3)
        Me.TabPage1.Controls.Add(Me.tbWoQty)
        Me.TabPage1.Controls.Add(Me.Label23)
        Me.TabPage1.Controls.Add(Me.Button1)
        Me.TabPage1.Controls.Add(Me.cbPackType)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.DataGridView1)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.tbItemNo)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.tbPartDesc)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.tbPartNo)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.tbWoCustCd)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.tbWoWoNo)
        Me.TabPage1.Location = New System.Drawing.Point(4, 25)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(921, 407)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "WO No   "
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(800, 9)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(101, 40)
        Me.Button5.TabIndex = 130
        Me.Button5.Text = "SELECT"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(800, 50)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(101, 40)
        Me.Button3.TabIndex = 129
        Me.Button3.Text = "ISSUE"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'tbWoQty
        '
        Me.tbWoQty.Location = New System.Drawing.Point(442, 8)
        Me.tbWoQty.Name = "tbWoQty"
        Me.tbWoQty.Size = New System.Drawing.Size(94, 22)
        Me.tbWoQty.TabIndex = 128
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(378, 11)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(60, 16)
        Me.Label23.TabIndex = 127
        Me.Label23.Text = "WO Qty :"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(800, 91)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(101, 40)
        Me.Button1.TabIndex = 55
        Me.Button1.Text = "START"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cbPackType
        '
        Me.cbPackType.FormattingEnabled = True
        Me.cbPackType.Location = New System.Drawing.Point(442, 38)
        Me.cbPackType.Name = "cbPackType"
        Me.cbPackType.Size = New System.Drawing.Size(233, 24)
        Me.cbPackType.TabIndex = 126
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(358, 42)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(80, 16)
        Me.Label11.TabIndex = 125
        Me.Label11.Text = "Pack Type :"
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.i_level, Me.i_upper_item_cd, Me.qty, Me.unit, Me.wi, Me.machine, Me.I_BATCH_SIZE_BUNSI, Me.I_BATCH_SIZE_BUNBO, Me.i_lower_item_cd, Me.I_SIZE_PER_ROOT, Me.ind_content})
        Me.DataGridView1.Location = New System.Drawing.Point(16, 138)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(885, 250)
        Me.DataGridView1.TabIndex = 124
        '
        'i_level
        '
        Me.i_level.DataPropertyName = "i_level"
        Me.i_level.HeaderText = "Level"
        Me.i_level.Name = "i_level"
        Me.i_level.Width = 80
        '
        'i_upper_item_cd
        '
        Me.i_upper_item_cd.DataPropertyName = "i_lower_item_cd"
        Me.i_upper_item_cd.HeaderText = "Item Cd"
        Me.i_upper_item_cd.Name = "i_upper_item_cd"
        Me.i_upper_item_cd.Width = 200
        '
        'qty
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.qty.DefaultCellStyle = DataGridViewCellStyle1
        Me.qty.HeaderText = "Qty"
        Me.qty.Name = "qty"
        '
        'unit
        '
        Me.unit.DataPropertyName = "i_unit_desc"
        Me.unit.HeaderText = "Unit"
        Me.unit.Name = "unit"
        '
        'wi
        '
        Me.wi.HeaderText = "WI"
        Me.wi.Name = "wi"
        Me.wi.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.wi.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.wi.Width = 150
        '
        'machine
        '
        Me.machine.HeaderText = "Machine"
        Me.machine.Name = "machine"
        Me.machine.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.machine.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.machine.Width = 150
        '
        'I_BATCH_SIZE_BUNSI
        '
        Me.I_BATCH_SIZE_BUNSI.DataPropertyName = "I_BATCH_SIZE_BUNSI"
        Me.I_BATCH_SIZE_BUNSI.HeaderText = "Bunsi"
        Me.I_BATCH_SIZE_BUNSI.Name = "I_BATCH_SIZE_BUNSI"
        Me.I_BATCH_SIZE_BUNSI.Visible = False
        '
        'I_BATCH_SIZE_BUNBO
        '
        Me.I_BATCH_SIZE_BUNBO.DataPropertyName = "I_BATCH_SIZE_BUNBO"
        Me.I_BATCH_SIZE_BUNBO.HeaderText = "Bunbo"
        Me.I_BATCH_SIZE_BUNBO.Name = "I_BATCH_SIZE_BUNBO"
        Me.I_BATCH_SIZE_BUNBO.Visible = False
        '
        'i_lower_item_cd
        '
        Me.i_lower_item_cd.HeaderText = "Lower Item"
        Me.i_lower_item_cd.Name = "i_lower_item_cd"
        Me.i_lower_item_cd.Visible = False
        '
        'I_SIZE_PER_ROOT
        '
        Me.I_SIZE_PER_ROOT.DataPropertyName = "I_SIZE_PER_ROOT"
        Me.I_SIZE_PER_ROOT.HeaderText = "Size Per Root"
        Me.I_SIZE_PER_ROOT.Name = "I_SIZE_PER_ROOT"
        Me.I_SIZE_PER_ROOT.Visible = False
        '
        'ind_content
        '
        Me.ind_content.DataPropertyName = "i_ind_content"
        Me.ind_content.HeaderText = "Ind Content"
        Me.ind_content.Name = "ind_content"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(70, 108)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 16)
        Me.Label6.TabIndex = 123
        Me.Label6.Text = "Item No :"
        '
        'tbItemNo
        '
        Me.tbItemNo.Location = New System.Drawing.Point(134, 105)
        Me.tbItemNo.Name = "tbItemNo"
        Me.tbItemNo.Size = New System.Drawing.Size(163, 22)
        Me.tbItemNo.TabIndex = 122
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(365, 75)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 16)
        Me.Label5.TabIndex = 121
        Me.Label5.Text = "Part Desc :"
        '
        'tbPartDesc
        '
        Me.tbPartDesc.Location = New System.Drawing.Point(442, 72)
        Me.tbPartDesc.Name = "tbPartDesc"
        Me.tbPartDesc.Size = New System.Drawing.Size(233, 22)
        Me.tbPartDesc.TabIndex = 120
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(71, 75)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 16)
        Me.Label4.TabIndex = 119
        Me.Label4.Text = "Part No :"
        '
        'tbPartNo
        '
        Me.tbPartNo.Location = New System.Drawing.Point(134, 72)
        Me.tbPartNo.Name = "tbPartNo"
        Me.tbPartNo.Size = New System.Drawing.Size(163, 22)
        Me.tbPartNo.TabIndex = 118
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label3.Location = New System.Drawing.Point(54, 43)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 16)
        Me.Label3.TabIndex = 117
        Me.Label3.Text = "Cust Code :"
        '
        'tbWoCustCd
        '
        Me.tbWoCustCd.Location = New System.Drawing.Point(134, 40)
        Me.tbWoCustCd.Name = "tbWoCustCd"
        Me.tbWoCustCd.Size = New System.Drawing.Size(163, 22)
        Me.tbWoCustCd.TabIndex = 116
        Me.tbWoCustCd.Text = "C001"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(72, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 16)
        Me.Label1.TabIndex = 115
        Me.Label1.Text = "WO No :"
        '
        'tbWoWoNo
        '
        Me.tbWoWoNo.Location = New System.Drawing.Point(134, 8)
        Me.tbWoWoNo.Name = "tbWoWoNo"
        Me.tbWoWoNo.Size = New System.Drawing.Size(163, 22)
        Me.tbWoWoNo.TabIndex = 114
        Me.tbWoWoNo.Text = "1602380724"
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.Button2)
        Me.TabPage5.Controls.Add(Me.btSelect)
        Me.TabPage5.Controls.Add(Me.dgWi)
        Me.TabPage5.Controls.Add(Me.Label8)
        Me.TabPage5.Controls.Add(Me.tbTab2WiNo)
        Me.TabPage5.Location = New System.Drawing.Point(4, 25)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(921, 407)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "WI No    "
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(802, 51)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(101, 40)
        Me.Button2.TabIndex = 127
        Me.Button2.Text = "ADD"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'btSelect
        '
        Me.btSelect.Location = New System.Drawing.Point(802, 9)
        Me.btSelect.Name = "btSelect"
        Me.btSelect.Size = New System.Drawing.Size(101, 40)
        Me.btSelect.TabIndex = 126
        Me.btSelect.Text = "SELECT"
        Me.btSelect.UseVisualStyleBackColor = True
        '
        'dgWi
        '
        Me.dgWi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgWi.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewComboBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.filename, Me.browse_file_row, Me.save_row, Me.wi_delete_row})
        Me.dgWi.Location = New System.Drawing.Point(18, 137)
        Me.dgWi.Name = "dgWi"
        Me.dgWi.Size = New System.Drawing.Size(885, 250)
        Me.dgWi.TabIndex = 125
        '
        'DataGridViewComboBoxColumn1
        '
        Me.DataGridViewComboBoxColumn1.DataPropertyName = "wi_no"
        Me.DataGridViewComboBoxColumn1.HeaderText = "WI"
        Me.DataGridViewComboBoxColumn1.Name = "DataGridViewComboBoxColumn1"
        Me.DataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewComboBoxColumn1.Width = 150
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "i_item_cd"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Item Cd"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 150
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "i_pack_cd"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Pack Type"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'filename
        '
        Me.filename.DataPropertyName = "wi_filename"
        Me.filename.HeaderText = "Filename"
        Me.filename.Name = "filename"
        Me.filename.Width = 150
        '
        'browse_file_row
        '
        Me.browse_file_row.HeaderText = "Browse"
        Me.browse_file_row.Name = "browse_file_row"
        Me.browse_file_row.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.browse_file_row.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.browse_file_row.Width = 80
        '
        'save_row
        '
        Me.save_row.HeaderText = "Save"
        Me.save_row.Name = "save_row"
        Me.save_row.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.save_row.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.save_row.Width = 80
        '
        'wi_delete_row
        '
        Me.wi_delete_row.HeaderText = "Delete"
        Me.wi_delete_row.Name = "wi_delete_row"
        Me.wi_delete_row.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.wi_delete_row.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.wi_delete_row.Width = 80
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(80, 11)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(51, 16)
        Me.Label8.TabIndex = 117
        Me.Label8.Text = "WI No :"
        '
        'tbTab2WiNo
        '
        Me.tbTab2WiNo.Location = New System.Drawing.Point(135, 8)
        Me.tbTab2WiNo.Name = "tbTab2WiNo"
        Me.tbTab2WiNo.Size = New System.Drawing.Size(163, 22)
        Me.tbTab2WiNo.TabIndex = 116
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btMcAdd)
        Me.TabPage2.Controls.Add(Me.btMcSelect)
        Me.TabPage2.Controls.Add(Me.dgMc)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.tbTab3McNo)
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(921, 407)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Machine"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btMcAdd
        '
        Me.btMcAdd.Location = New System.Drawing.Point(802, 51)
        Me.btMcAdd.Name = "btMcAdd"
        Me.btMcAdd.Size = New System.Drawing.Size(101, 40)
        Me.btMcAdd.TabIndex = 130
        Me.btMcAdd.Text = "ADD"
        Me.btMcAdd.UseVisualStyleBackColor = True
        '
        'btMcSelect
        '
        Me.btMcSelect.Location = New System.Drawing.Point(802, 9)
        Me.btMcSelect.Name = "btMcSelect"
        Me.btMcSelect.Size = New System.Drawing.Size(101, 40)
        Me.btMcSelect.TabIndex = 129
        Me.btMcSelect.Text = "SELECT"
        Me.btMcSelect.UseVisualStyleBackColor = True
        '
        'dgMc
        '
        Me.dgMc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgMc.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn4, Me.ind_contents, Me.ind_cont_desc, Me.DataGridViewTextBoxColumn5, Me.begin_lot, Me.mc_save_row, Me.mc_delete_row})
        Me.dgMc.Location = New System.Drawing.Point(18, 137)
        Me.dgMc.Name = "dgMc"
        Me.dgMc.Size = New System.Drawing.Size(885, 250)
        Me.dgMc.TabIndex = 126
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "mc_cd"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Mach Cd"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "mc_desc"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Mach Desc"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'ind_contents
        '
        Me.ind_contents.DataPropertyName = "ind_contents"
        Me.ind_contents.HeaderText = "Ind Cont"
        Me.ind_contents.Name = "ind_contents"
        '
        'ind_cont_desc
        '
        Me.ind_cont_desc.DataPropertyName = "ind_cont_desc"
        Me.ind_cont_desc.HeaderText = "Ind Cont Desc"
        Me.ind_cont_desc.Name = "ind_cont_desc"
        Me.ind_cont_desc.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "mc_status"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'begin_lot
        '
        Me.begin_lot.DataPropertyName = "begin_lot"
        Me.begin_lot.HeaderText = "Begin Lot"
        Me.begin_lot.Name = "begin_lot"
        '
        'mc_save_row
        '
        Me.mc_save_row.HeaderText = "Save"
        Me.mc_save_row.Name = "mc_save_row"
        Me.mc_save_row.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.mc_save_row.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.mc_save_row.Width = 80
        '
        'mc_delete_row
        '
        Me.mc_delete_row.HeaderText = "Delete"
        Me.mc_delete_row.Name = "mc_delete_row"
        Me.mc_delete_row.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.mc_delete_row.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.mc_delete_row.Width = 80
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label7.Location = New System.Drawing.Point(42, 11)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(86, 16)
        Me.Label7.TabIndex = 119
        Me.Label7.Text = "Machine No :"
        '
        'tbTab3McNo
        '
        Me.tbTab3McNo.Location = New System.Drawing.Point(132, 8)
        Me.tbTab3McNo.Name = "tbTab3McNo"
        Me.tbTab3McNo.Size = New System.Drawing.Size(163, 22)
        Me.tbTab3McNo.TabIndex = 118
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.cbLineInMat)
        Me.TabPage4.Controls.Add(Me.Label9)
        Me.TabPage4.Controls.Add(Me.Label12)
        Me.TabPage4.Controls.Add(Me.tbMatWoNo)
        Me.TabPage4.Controls.Add(Me.Button15)
        Me.TabPage4.Controls.Add(Me.Button12)
        Me.TabPage4.Controls.Add(Me.dgMaterial)
        Me.TabPage4.Location = New System.Drawing.Point(4, 25)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(921, 407)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Material"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'cbLineInMat
        '
        Me.cbLineInMat.FormattingEnabled = True
        Me.cbLineInMat.Location = New System.Drawing.Point(134, 9)
        Me.cbLineInMat.Name = "cbLineInMat"
        Me.cbLineInMat.Size = New System.Drawing.Size(52, 24)
        Me.cbLineInMat.TabIndex = 142
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label9.Location = New System.Drawing.Point(91, 13)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(39, 16)
        Me.Label9.TabIndex = 141
        Me.Label9.Text = "Line :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label12.Location = New System.Drawing.Point(72, 44)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(58, 16)
        Me.Label12.TabIndex = 140
        Me.Label12.Text = "WO No :"
        '
        'tbMatWoNo
        '
        Me.tbMatWoNo.Location = New System.Drawing.Point(134, 41)
        Me.tbMatWoNo.Name = "tbMatWoNo"
        Me.tbMatWoNo.Size = New System.Drawing.Size(163, 22)
        Me.tbMatWoNo.TabIndex = 139
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(802, 49)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(101, 40)
        Me.Button15.TabIndex = 138
        Me.Button15.Text = "ISSUE"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(802, 8)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(101, 40)
        Me.Button12.TabIndex = 135
        Me.Button12.Text = "SELECT"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'dgMaterial
        '
        Me.dgMaterial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgMaterial.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.wo, Me.DataGridViewTextBoxColumn11, Me.I_SIM_ITEM_DESC, Me.lot_no, Me.DataGridViewTextBoxColumn13, Me.act_qty, Me.Column2, Me.Location, Me.remarks, Me.mat_delete_row})
        Me.dgMaterial.Location = New System.Drawing.Point(18, 137)
        Me.dgMaterial.Name = "dgMaterial"
        Me.dgMaterial.Size = New System.Drawing.Size(885, 250)
        Me.dgMaterial.TabIndex = 126
        '
        'OpenFileDialogWi
        '
        Me.OpenFileDialogWi.FileName = "OpenFileDialogWi"
        '
        'wo
        '
        Me.wo.DataPropertyName = "I_PO_DETAIL_NO"
        Me.wo.HeaderText = "WO No"
        Me.wo.Name = "wo"
        Me.wo.Width = 120
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "I_SIM_ITEM_CD"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Mat Cd"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Width = 120
        '
        'I_SIM_ITEM_DESC
        '
        Me.I_SIM_ITEM_DESC.DataPropertyName = "I_SIM_ITEM_DESC"
        Me.I_SIM_ITEM_DESC.HeaderText = "Mat Desc"
        Me.I_SIM_ITEM_DESC.Name = "I_SIM_ITEM_DESC"
        Me.I_SIM_ITEM_DESC.Width = 200
        '
        'lot_no
        '
        Me.lot_no.DataPropertyName = "lot_no"
        Me.lot_no.HeaderText = "Lot No"
        Me.lot_no.Name = "lot_no"
        Me.lot_no.Width = 120
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "bom_qty"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Bom Qty"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        '
        'act_qty
        '
        Me.act_qty.DataPropertyName = "act_qty"
        Me.act_qty.HeaderText = "Act Qty"
        Me.act_qty.Name = "act_qty"
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "I_SIM_STD_UNIT_DESC"
        Me.Column2.HeaderText = "Unit"
        Me.Column2.Name = "Column2"
        '
        'Location
        '
        Me.Location.HeaderText = "Location"
        Me.Location.Name = "Location"
        Me.Location.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Location.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'remarks
        '
        Me.remarks.HeaderText = "Remarks"
        Me.remarks.Name = "remarks"
        Me.remarks.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.remarks.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.remarks.Width = 200
        '
        'mat_delete_row
        '
        Me.mat_delete_row.HeaderText = "Delete"
        Me.mat_delete_row.Name = "mat_delete_row"
        Me.mat_delete_row.Text = "DELETE"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(953, 511)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Nitto_Logo)
        Me.Name = "Form1"
        Me.Text = "Production Control V1.1.0"
        CType(Me.Nitto_Logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        CType(Me.dgWi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dgMc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.dgMaterial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Nitto_Logo As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbWoWoNo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbWoCustCd As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbPartNo As System.Windows.Forms.TextBox
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbPartDesc As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbTab2WiNo As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbItemNo As System.Windows.Forms.TextBox
    Friend WithEvents dgWi As System.Windows.Forms.DataGridView
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents btSelect As System.Windows.Forms.Button
    Friend WithEvents dgMc As System.Windows.Forms.DataGridView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tbTab3McNo As System.Windows.Forms.TextBox
    Friend WithEvents btMcAdd As System.Windows.Forms.Button
    Friend WithEvents btMcSelect As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cbPackType As System.Windows.Forms.ComboBox
    Friend WithEvents dgMaterial As System.Windows.Forms.DataGridView
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents tbMatWoNo As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialogWi As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ind_contents As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ind_cont_desc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents begin_lot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents mc_save_row As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents mc_delete_row As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents DataGridViewComboBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents filename As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents browse_file_row As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents save_row As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents wi_delete_row As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents tbWoQty As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents i_level As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents i_upper_item_cd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents qty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents unit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents wi As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents machine As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents I_BATCH_SIZE_BUNSI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents I_BATCH_SIZE_BUNBO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents i_lower_item_cd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents I_SIZE_PER_ROOT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ind_content As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cbLineInMat As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents wo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents I_SIM_ITEM_DESC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lot_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents act_qty As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Location As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents remarks As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents mat_delete_row As System.Windows.Forms.DataGridViewButtonColumn

End Class
